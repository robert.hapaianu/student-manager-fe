export interface Student {
  id: number;
  name: string;
  email: string;
  phone: string;
  imageUrl: string;
  faculty: string;
  specialization: string;
  averageGrade: number;
  studentCode: string;
}
